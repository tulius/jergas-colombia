# Jergas Colombianas

<img alt="Logo" src="images/Jergas_Colombia_logo.png" width="96" />

(Plataforma: Android 5.0 Lollipop hasta la Versión 13.0 )

## Acerca

Jergas Colombia, es útil para residentes en Colombia y extranjeros ya que permite consultar las jergas mas usadas en Colombia.

1) Si eres residente podrás entender a los locales de otras regiones.

2) Para turistas o extranjeros(as) residentes será de gran ayuda para mantener un diálogo mas fluido con los Colombianos.

La jerga es el uso de palabras y expresiones informales que no se consideran estándar en el dialecto o idioma del hablante.

Aparte de jergas, también encontrará gran cantidad de elementos indígenas y del español antiguo, Como es el caso del departamento del Tolima.

## Screenshots

<div style="display:flex;">
<img alt="App image" src="metadata/es-MX/images/phoneScreenshots/captura-1.png" width="30%">
<img alt="App image" src="metadata/es-MX/images/phoneScreenshots/captura-2.png" width="30%">
</div>



## Recomendación

+    A pesar de que gran parte de estas palabras están aceptadas por la RAE, se recomienda a los locales usar palabras del español estándar, en los diálogos con turistas.


## Privacidad

Jergas Colombia:

1. No obtiene información privada como contactos, tendencias, búsquedas web etc.

2. No requiere conexión a internet.

3. No usa y por ningún motivo usará publicidad.

4. No captura información privada y no tiene implementado ningún tipo de rastreador.


## Garantía

Este programa se distribuye con la esperanza de que sea útil pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de MERCANTIBILIDAD o CALIFICADA PARA UN PROPÓSITO EN PARTICULAR. Vea la Licencia General Pública de GNU para más detalles.

Usted ha debido de recibir una copia de la Licencia General Pública de GNU junto con este programa. Si no, vea:
[GPLv3 en Español](https://raw.githubusercontent.com/LSLSpanish/translation_GPLv3_to_spanish/master/Versi%C3%B3n%20definitiva/GPLv3-spanish.pdf)


## Última liberación

**Versión 1.0**  

<a href="app/release/jergasColombia_1.0.apk" rel="nofollow"> <img src="images/descargar-aqui.png" alt="Descargar" ></a>  

<a href="https://f-droid.org/es/packages/app.tujice.jergasColombia/" rel="nofollow" target="_blank"> <img src="images/ObtenerEn_f-droid_240.png" alt="Obtenerlo en F-Droid" ></a>  
