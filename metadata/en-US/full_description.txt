<b>Jergas Colombia is a brief Colombian dictionary.</b>

- Useful for residents in Colombia and foreigners since it allows to consult the most used jargons in Colombia-South America.

- Slang is the use of informal words and expressions that are not considered standard in the speaker's dialect or language.

- In addition to jargon, you will also find a large number of indigenous and old Spanish elements, as is the case in the department of Tolima.

- Jergas Colombia will always be at your fingertips without requiring internet.

- If you are a resident you will be able to understand locals from other regions of Colombia.

- For tourists or foreign residents it will be a great help to maintain a more fluid dialogue with Colombians.
