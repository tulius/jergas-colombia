package app.tujice.jergasColombia;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        TextView tvPalabras = findViewById(R.id.tvPalabras);
        TextView tvFrases = findViewById(R.id.tvFrases);
        tvPalabras.setOnClickListener(this);
        tvFrases.setOnClickListener(this);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuCreditos) {
            Intent creditos = new Intent(this, CreditosActivity.class);
            this.finish();
            startActivity(creditos);
        }
        if (id == R.id.menuAcerca) {
            Intent i = new Intent(this, AcercaActivity.class);
            this.finish();
            startActivity(i);
        }
        if (id == R.id.menuSalir) {
            finishAndRemoveTask();
            System.exit(0);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvPalabras) {
            evento_palabras(v);
        }
        if (v.getId() == R.id.tvFrases) {
            evento_frases(v);
        }
    }

    public void evento_palabras(View view) {
        System.out.println("*** evento_palabras ** ");
         Bundle extras = new Bundle();
        // En DefinicionesActivity se recibe el valor mediante getString()
        extras.putString("variable_string", "palabras");
        Intent i = new Intent(this, DefinicionesActivity.class);
        i.putExtras(extras);
        this.finish();
        startActivity(i);
    }

    public void evento_frases(View view) {
        System.out.println("*** evento_frases ** ");
         Bundle extras = new Bundle();
        // En DefinicionesActivity se recibe el valor mediante getString() en "AyudaActivity.java"
        extras.putString("variable_string", "frases");
        Intent i = new Intent(this, DefinicionesActivity.class);
        i.putExtras(extras);
        this.finish();
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        System.out.println("*** Ha pulsado atrás ** ");
        super.onBackPressed();
    }

}
