package app.tujice.jergasColombia;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;


public class DefinicionesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.definiciones);

       //setTitle("");

        System.out.println("*** en DefinicionesActivity *** ");
        WebView visorDefiniciones =  findViewById(R.id.visorDefiniciones);
        WebSettings webSettings = visorDefiniciones.getSettings();

        // Para que acepte scripts web, puede producir vulnerabilidades
        webSettings.setJavaScriptEnabled(true);

         // Recuperar los datos enviados desde MainActiviy
        String datos_recuperados;
        datos_recuperados = getIntent().getStringExtra("variable_string");
        System.out.println(" > datos_recuperados: "+datos_recuperados);

        if (datos_recuperados.equalsIgnoreCase("palabras")) {
            visorDefiniciones.loadUrl("file:///android_asset/Palabras.html");
            setTitle("Palabras");
        }
        if (datos_recuperados.equalsIgnoreCase("frases")) {
            visorDefiniciones.loadUrl("file:///android_asset/Frases.html");
            setTitle("Frases");
        }

    }

      @Override
    public void onBackPressed() {
        //Prueba a dejar esto en blanco, o a meter en el sharedpreference de
        //nuevo la variable que no quieres que se borre aqui
        System.out.println("*** Ha pulsado atrás ** ");
        Intent palabras = new Intent(this, MainActivity.class);
        this.finish();
        startActivity(palabras);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_definiciones, menu);
        return true;

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuInicio) {
            Intent inicio = new Intent(this, MainActivity.class);
            this.finish();
            startActivity(inicio);
        }
        if (id == R.id.menuSalir) {
            finishAndRemoveTask();
            System.exit(0);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
