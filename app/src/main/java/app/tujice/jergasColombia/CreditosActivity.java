package app.tujice.jergasColombia;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

//public class CreditosActivity extends Activity {
public class CreditosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creditos);

        setTitle("Créditos");
        System.out.println("*** CréditosActivity ** ");

        WebView visorCreditos =  findViewById(R.id.visorCreditos);
        // WebSettings webSettings = visorCreditos.getSettings();

        visorCreditos.loadUrl("file:///android_asset/Creditos.html");
        setTitle("Créditos");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_volver, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menuVolver) {
            Intent inicio = new Intent(this, MainActivity.class);
            this.finish();
            startActivity(inicio);
        }
        if (id == R.id.menuVolverText) {
            Intent inicio = new Intent(this, MainActivity.class);
            this.finish();
            startActivity(inicio);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        System.out.println("*** onBackPressed ** ");
        Intent inicio = new Intent(this, MainActivity.class);
        startActivity(inicio);
        finish();
    }

}


